## Setup

```sh
pyenv virtualenv 3.7.4 som-office-provisioning
pyenv exec pip install -r requirements.txt
pyenv exec ansible-galaxy install -r requirements.yml
```

## Development

* install devenv https://github.com/coopdevs/devenv
* run the following

```sh
    cd /path/to/somoffice-provisioning
    devenv
    pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=dev -u root --ask-vault-pass
    pyenv exec ansible-playbook playbooks/provision.yml --limit=dev --ask-vault-pass
```

> Password in BW: `somoffice-provisioning dev ansible secrets`

## Staging
```sh
pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=staging -u root --ask-vault-pass
pyenv exec ansible-playbook playbooks/provision.yml --limit=staging --ask-vault-pass
pyenv exec ansible-playbook playbooks/deploy.yml --limit=staging --ask-vault-pass -e ansistrano_git_branch=testing/2022-01-01
```

> Password in BW: `somoffice-provisioning staging ansible secrets`

## Production
```sh
pyenv exec ansible-playbook playbooks/sys_admins.yml --limit=production -u root --ask-vault-pass
pyenv exec ansible-playbook playbooks/provision.yml --limit=production --ask-vault-pass
pyenv exec ansible-playbook playbooks/deploy.yml --limit=production --ask-vault-pass -e ansistrano_git_branch=v0.0.0
```

> Password in BW: `somoffice-provisioning production ansible secrets`


## Show maintenance alert (temporal solution)

Sometimes, we need to alert the somoffice customers that the service cannot be used for a while (when implementing updates, if related servers are down, etc).

Temporary, since the solution we present here is to be improved, we can follow these steps to block the server services showing the maintenance message.

  1. Move to the `somoffice` project folder (cloned version from https://gitlab.com/coopdevs/somoffice), making sure we have its latest commits with git pull.
  2. Create a new branch from master (<new-maintenance-branch>, ex: 'maintenance-2022-02-02')
  3. Set `isMantenanceMode=true` editing the file `fronent/src/App.js`.
  4. Git push the new local branch to the repository
  5. Deploy it to the corresponding <server> ['staging', 'production']
  6. pyenv exec ansible-playbook playbooks/deploy.yml --limit=<server> --ask-vault-pass -e ansistrano_git_branch=<new-maintenance-branch>

Once the deploy finishes, the somoffice service will already be in maintenance mode.

When we are ready to put the server back on use, we can deploy again (step 6) using the `master` branch.